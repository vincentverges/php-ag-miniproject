# php-ag-miniproject

## Install :

Clone the project from this [project](https://gitlab.com/Winciwizard/php-ag-miniproject) 

`https://gitlab.com/Winciwizard/php-ag-miniproject.git`

After enter to the source folder of the project :

* Make a `composer install`
* `cp .env.dist .env` 
* Launch the docker-compose.yml `docker-compose up -d`
* Update the .env file with your local credentials with you PostgreSQL database
* create the database ` php bin/console doctrine:database:create`
* Make the migration with `php bin/console doctrine:migrations:migrate`
* Run the server `php bin/console server:run`

## Routes :

#### GET "/product" :

Find all products

#### GET "/product/{id}" :

Find product with this {id}

#### POST "/product/new" :

Insert a new product. 

The template of the product for the request body: 

```
{
	"rawLabel": "Nutella 1kg",
	"quantity": "1",
	"unitPrice": "3.64",
	"packageUnity": "Kg",
	"mass": "1",
	"flag": [],
	"price": "3.64",
	"productData": {
		"name": "Nutella",
		"averagePrice": "4.02",
		"category": "SWEET_GROCERY",
		"brand": "Ferrero"
	}
}

```

#### POST "/product/{id}/edit" :

Update the product with this {id}

You can add only needed value to change in the body, but respect the strucutre, like that :

```angular2html
{
    "price": "3.64",
    "productData": {
		"name": "Nutella"
		}
}
```

#### DELETE "/product/{id}"

Delete product with this {id}


## How it work

When you add a product with POST "/product/new" route, the controller call a service "MiniProjectService". This service send the product object to a root of kotlin-kotlin-miniprojet.

A singleton is using at the beginning to make the JWT auth to kotlin-miniprojet, the token will be store in a Session.

After this it add product to the database.