<?php
/**
 * Created by PhpStorm.
 * User: aboutgoods
 * Date: 05/04/19
 * Time: 14:58
 */

namespace App\Service;


use App\Entity\Product;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Serializer\SerializerInterface;

class MiniProjectService
{

    private $client;
    private $session;
    private $serializer;


    function __construct(SessionInterface $session, SerializerInterface $serializer)
    {
        $this->client = new Client();
        $this->session = $session;
        $this->serializer = $serializer;
        $this->login();

    }

    /**
     * @param Product $product
     *
     * To send a product to the kotlin-ag-project application
     */
    public function sendProduct(Product $product) {

        $data = $this->serializer->serialize($product,'json');
        $this->isConnected();

        $token = 'Bearer '.$this->session->get('token');
        try {

            //Todo: uri in .env
            $response = $this->client->request(
                'POST',
                'http://0.0.0.0:8080/product',
                [
                    'headers' => [
                        'content-type' => 'application/json',
                        'authorization' => $token
                    ],
                    'body' => $data
                ]);
            dd($response->getBody()->getContents());
        } catch (GuzzleException $e) {
        }
    }

    /**
     * Log to the kotlin-ag-project application
     */
    private function login() {

        $data = [
            'user' => 'test',
            'password' => 'test'
        ];

        try {

            //Todo: uri in .env
            $response = $this->client->request(
                'POST',
                'http://0.0.0.0:8080/login-register',
                [
                    'headers' => ['content-type' => 'application/json'],
                    'body' => json_encode($data)
                ]);


            $responseData = json_decode($response->getBody()->getContents());
            $this->session->set('token', $responseData->token);

        } catch (GuzzleException $e) {
        }
    }


    private  function isConnected(){
        if($this->session->get('token') == null) {
            $this->login();
        }
    }

    /**
     * @return SessionInterface
     */
    public function getSession(): SessionInterface
    {
        return $this->session;
    }


}