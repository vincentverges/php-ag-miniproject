<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190404135251 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE product_data_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE product_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE product_data (id INT NOT NULL, name VARCHAR(255) DEFAULT NULL, average_price VARCHAR(255) DEFAULT NULL, category VARCHAR(255) DEFAULT NULL, brand VARCHAR(255) DEFAULT NULL, gtin VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE product (id INT NOT NULL, raw_label VARCHAR(255) DEFAULT NULL, short_label VARCHAR(255) DEFAULT NULL, quantity VARCHAR(255) DEFAULT NULL, unit_price VARCHAR(255) DEFAULT NULL, package_unity VARCHAR(255) DEFAULT NULL, mass VARCHAR(255) DEFAULT NULL, flag TEXT DEFAULT NULL, price VARCHAR(255) DEFAULT NULL, rules TEXT DEFAULT NULL, product_id INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('COMMENT ON COLUMN product.flag IS \'(DC2Type:array)\'');
        $this->addSql('COMMENT ON COLUMN product.rules IS \'(DC2Type:array)\'');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP SEQUENCE product_data_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE product_id_seq CASCADE');
        $this->addSql('DROP TABLE product_data');
        $this->addSql('DROP TABLE product');
    }
}
