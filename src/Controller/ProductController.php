<?php

namespace App\Controller;

use App\Entity\Product;
use App\Entity\ProductData;
use App\Form\ProductType;
use App\Repository\ProductRepository;
use App\Service\MiniProjectService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

/**
 * @Route("/product")
 */
class ProductController extends AbstractController
{
    /**
     * @Route("/", name="product_index", methods={"GET"})
     * @param ProductRepository $productRepository
     * @return Response
     *
     * The route to find all products
     */
    public function index(ProductRepository $productRepository): Response
    {
        return $this->json(
            ['products' => $productRepository->findAll()]
        );
    }

    /**
     * @Route("/new", name="product_new", methods={"POST"})
     * @param Request $request
     * @param MiniProjectService $miniProject
     * @return Response
     *
     * The route to create a new product
     */
    public function new(Request $request, MiniProjectService $miniProject): Response
    {
        $content = json_decode($request->getContent());

        $productData = new ProductData();
        $productData->setName($content->productData->name);
        $productData->setAveragePrice($content->productData->averagePrice);
        $productData->setCategory($content->productData->category);
        $productData->setbrand($content->productData->brand);

        $product = new Product;
        $product->setRawLabel($content->rawLabel);
        $product->setShortLabel($content->rawLabel);
        $product->setQuantity($content->quantity);
        $product->setUnitPrice($content->unitPrice);
        $product->setPackageUnity($content->packageUnity);
        $product->setMass($content->mass);
        $product->setFlag($content->flag);
        $product->setPrice($content->price);
        $product->setProductData($productData);

        //Send product to koltin-ag-miniproject
        $miniProject->sendProduct($product);

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($product);
        $entityManager->flush();

        return $this->json(
            ['product' => $product]
        );
    }

    /**
     * @Route("/{id}", name="product_show", methods={"GET"})
     * @param Product $product
     * @return Response
     *
     * The route to find specific product
     */
    public function show(Product $product): Response
    {
        return $this->json([
            'product' => $product,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="product_edit", methods={"POST"})
     * @param Request $request
     * @param Product $product
     * @return Response
     *
     * The route to edit specific product
     */
    public function edit(Request $request, Product $product): Response
    {
        $productData = $product->getProductData();
        $content = json_decode($request->getContent());


        //We can send only change, so we need to check
        if(isset($content->rawLabel)) {
            $product->setRawLabel($content->rawLabel);
            $product->setShortLabel($content->rawLabel);
        }
        if(isset($content->shortLabel)) {
            $product->setShortLabel($content->shortLabel);
        }
        if(isset($content->quantity)) {
            $product->setQuantity($content->quantity);
        }
        if(isset($content->unitPrice)) {
            $product->setUnitPrice($content->unitPrice);
        }
        if(isset($content->packageUnity)) {
            $product->setPackageUnity($content->packageUnity);
        }
        if(isset($content->mass)) {
            $product->setMass($content->mass);
        }
        if(isset($content->flag)) {
            $product->setMass($content->flag);
        }
        if(isset($content->price)) {
            $product->setPrice($content->price);
        }
        if(isset($content->productData)) {

            if(isset($content->productData->name)) {
                $productData->setName($content->productData->name);
            }
            if(isset($content->productData->averagePrice)) {
                $productData->setAveragePrice($content->productData->averagePrice);
            }
            if(isset($content->productData->category)) {
                $productData->setCategory($content->productData->category);
            }
            if(isset($content->productData->brand)) {
                $productData->setBrand($content->productData->brand);
            }
            if(isset($content->productData->gtin)) {
                $productData->setGtin($content->productData->gtin);
            }

            $product->setProductData($productData);
        }

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($product);
        $entityManager->flush();

        return $this->json(
            [ 'product' => $product]
        );
    }

    /**
     * @Route("/{id}", name="product_delete", methods={"DELETE"})

     * @param Product $product
     * @return Response
     *
     * The route to delete specific product
     */
    public function delete(Product $product): Response
    {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($product);
            $entityManager->flush();

        return $this->json('product deleted');
    }
}
