<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProductRepository")
 */
class Product
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $rawLabel;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $shortLabel;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $quantity;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $unitPrice;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $packageUnity;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $mass;

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private $flag = [];

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $price;

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private $rules = [];

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ProductData", inversedBy="product", cascade={"persist", "remove"})
     */
    private $productData;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRawLabel(): ?string
    {
        return $this->rawLabel;
    }

    public function setRawLabel(?string $rawLabel): self
    {
        $this->rawLabel = $rawLabel;

        return $this;
    }

    public function getShortLabel(): ?string
    {
        return $this->shortLabel;
    }

    public function setShortLabel(?string $shortLabel): self
    {
        $this->shortLabel = $shortLabel;

        return $this;
    }

    public function getQuantity(): ?string
    {
        return $this->quantity;
    }

    public function setQuantity(?string $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }

    public function getUnitPrice(): ?string
    {
        return $this->unitPrice;
    }

    public function setUnitPrice(?string $unitPrice): self
    {
        $this->unitPrice = $unitPrice;

        return $this;
    }

    public function getPackageUnity(): ?string
    {
        return $this->packageUnity;
    }

    public function setPackageUnity(?string $packageUnity): self
    {
        $this->packageUnity = $packageUnity;

        return $this;
    }

    public function getMass(): ?string
    {
        return $this->mass;
    }

    public function setMass(?string $mass): self
    {
        $this->mass = $mass;

        return $this;
    }

    public function getFlag(): ?array
    {
        return $this->flag;
    }

    public function setFlag(?array $flag): self
    {
        $this->flag = $flag;

        return $this;
    }

    public function getPrice(): ?string
    {
        return $this->price;
    }

    public function setPrice(?string $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getRules(): ?array
    {
        return $this->rules;
    }

    public function setRules(?array $rules): self
    {
        $this->rules = $rules;

        return $this;
    }

    public function getProductData(): ?ProductData
    {
        return $this->productData;
    }

    public function setProductData(?ProductData $productData): self
    {
        $this->productData = $productData;

        return $this;
    }
}
